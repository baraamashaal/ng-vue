var elixir = require('laravel-elixir');
require('laravel-elixir-jade');

require('laravel-elixir-wiredep');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix
	    .wiredep('sass')
	    .wiredep('jade',{
	    	baseDir: 'resources/assets/jade/layout',
    		src: 'master.jade',
    		// searchLevel: '**/*.jade'
	    	},{
      block: /(([ \t]*)\/\/\s*bower:*(\S*))(\n|\r|.)*?(\/\/\s*endbower)/gi,
      detect: {
        js: /script\(.*src=['"]([^'"]+)/gi,
        css: /link\(.*href=['"]([^'"]+)/gi
      },
      replace: {
        js: 'script(src=\'{{filePath}}\')',
        css: 'link(rel=\'stylesheet\', href=\'{{filePath}}\')'
      }
    })
    	.sass('app.scss')
    	.coffee('app.coffee')
	    .jade({
	    	src: '/assets/jade/'
	    	})
    	.browserSync({
	        proxy: 'localhost:8000'
	    });

});

