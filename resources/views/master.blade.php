
<html lang="en"></html>
<head>
  <meta charset="utf-8"/>
  <link rel="stylesheet" href="css/app.css"/>
  <title>test.</title>
  <!-- bower:css-->
  <!-- endbower-->
  <body>
    <script src="js/app.js"></script>
    <!-- bower:js-->
    <script src="/bower_components/jquery/dist/jquery.js"></script>
    <!-- endbower-->
  </body>
</head>